package org.myorg;

import java.io.IOException;
import java.util.*;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;

public class MatrixMultiV1 {

   public static class Map extends MapReduceBase implements Mapper<LongWritable, Text, Text, Text> {

       private Text key_out = new Text();
       private Text val_out = new Text();


       public void map(LongWritable key, Text value, OutputCollector<Text, Text> output, Reporter reporter) throws IOException {
           String line = value.toString();
           String[] parts = line.split("\\s+");
           String label = parts[0];
           String n_str = parts[1];
           int n = Integer.parseInt(n_str);

           boolean swap_index = false;
           if (label.equals("A")) swap_index = true;

           String i_str = parts[2];
           int i = Integer.parseInt(i_str);

           for (int j = 0; j < n; ++j) {
               String key_out_str = swap_index ? j + " " + i : i + " " + j;
               for (int k = 0; k < n; ++k) {
                   String val_out_str = label + " " + k + " " + parts[3 + k];
                   key_out.set(key_out_str);
                   val_out.set(val_out_str);
                   output.collect(key_out, val_out);
               }
           }
       }
   }

    public static class Reduce extends MapReduceBase implements Reducer<Text, Text, Text, Text> {

        private Text val_out = new Text();


        public void reduce(Text key, Iterator<Text> values, OutputCollector<Text, Text> output, Reporter reporter) throws IOException {
            List<String> cache = new ArrayList<String>();

            while (values.hasNext()) {
                cache.add(values.next().toString());
            }

            final int n = cache.size() / 2;

            Collections.sort(cache);

            double sum = 0;
            for (int i = 0; i < n; ++i) {
                String matrix_1_val_str = cache.get(0 + i);
                String matrix_2_val_str = cache.get(n + i);
                String[] matrix_1_val_parts = matrix_1_val_str.split("\\s+");
                String[] matrix_2_val_parts = matrix_2_val_str.split("\\s+");

                double matrix_1_val = Double.parseDouble(matrix_1_val_parts[2]);
                double matrix_2_val = Double.parseDouble(matrix_2_val_parts[2]);

                sum += matrix_1_val * matrix_2_val;
            }

            val_out.set("" + sum);
            output.collect(key, val_out);
        }
    }


   public static void main(String[] args) throws Exception {
     JobConf conf = new JobConf(MatrixMultiV1.class);
     conf.setJobName("matrix_multi_v1");

     conf.setOutputKeyClass(Text.class);
     conf.setOutputValueClass(Text.class);

     conf.setMapperClass(Map.class);
     conf.setCombinerClass(Reduce.class);
     //conf.setReducerClass(Reduce.class);

     conf.setInputFormat(TextInputFormat.class);
     conf.setOutputFormat(TextOutputFormat.class);

     FileInputFormat.setInputPaths(conf, new Path(args[0]));
     FileOutputFormat.setOutputPath(conf, new Path(args[1]));

     JobClient.runJob(conf);
   }
}
