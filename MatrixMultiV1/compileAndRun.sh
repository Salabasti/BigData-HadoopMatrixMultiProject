#!/bin/bash
javac -classpath /usr/lib/hadoop-mapreduce/*:/usr/lib/hadoop/* -d MatrixMultiV1_classes MatrixMultiV1.java
jar -cvf MatrixMultiV1.jar -C MatrixMultiV1_classes/ .
hdfs dfs -rm -r matrix_out
hadoop jar MatrixMultiV1.jar org.myorg.MatrixMultiV1 matrix_in matrix_out
