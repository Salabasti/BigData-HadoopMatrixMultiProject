package org.myorg;
 	
import java.io.IOException;
import java.util.*;
 	
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;
 	
public class MatrixMultiV1 {
 	
   public static class Map extends MapReduceBase implements Mapper<LongWritable, Text, Text, Text> {

       private Text key_out = new Text();
       private Text val_out = new Text();
       
       
       public void map(LongWritable key, Text value, OutputCollector<Text, Text> output, Reporter reporter) throws IOException {
           String line = value.toString();
         
           String[] parts = line.split("\\s+");
           String label = parts[0];
           String n_str = parts[1];
           int n = Integer.parseInt(n_str);
           String i_str = parts[2];
           int i = Integer.parseInt(i_str);
           String[] row_vals_str = Arrays.copyOfRange(parts, 3, n - 1);
         
           for (int j = 0; j < n; ++j) {
               String key_out_str = i + " " + j;
             
               for (int k = 0; k < n; ++k) {
                   String val_out_str = label + " " + k + " " + row_vals_str[k];
                   key_out.set(key_out_str);
                   val_out.set(val_out_str);
                   output.collect(key_out, val_out);
               }
           }
       /*String line = value.toString();
       StringTokenizer tokenizer = new StringTokenizer(line);
       while (tokenizer.hasMoreTokens()) {
         word.set(tokenizer.nextToken());
         output.collect(word, one);
       }*/
     }
   }
 	
   public static class Reduce extends MapReduceBase implements Reducer<Text, IntWritable, Text, IntWritable> {
     public void reduce(Text key, Iterator<IntWritable> values, OutputCollector<Text, IntWritable> output, Reporter reporter) throws IOException {
       /*int sum = 0;
       while (values.hasNext()) {
         sum += values.next().get();
       }
       output.collect(key, new IntWritable(sum));*/
     }
   }
 	
   public static void main(String[] args) throws Exception {
     JobConf conf = new JobConf(WordCount.class);
     conf.setJobName("matrix_multi_v1");
 	
     conf.setOutputKeyClass(Text.class);
     conf.setOutputValueClass(Text.class);

     conf.setMapperClass(Map.class);
     //conf.setCombinerClass(Reducer.class);
     conf.setReducerClass(Reduce.class);
 	
     conf.setInputFormat(TextInputFormat.class);
     conf.setOutputFormat(TextOutputFormat.class);
 	
     FileInputFormat.setInputPaths(conf, new Path(args[0]));
     FileOutputFormat.setOutputPath(conf, new Path(args[1]));
 	
     JobClient.runJob(conf);
   }
}
