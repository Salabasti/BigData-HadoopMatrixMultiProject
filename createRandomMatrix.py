#!/usr/bin/python


import numpy as np
import sys, getopt


def main(argv):
    n_str = '0'
    transpose = 0
    label = 'X'
    verbose = 0
    algo = "rand"
    
    try:
        opts, args = getopt.getopt(argv,"vthn:l:a:",["n="])
    except getopt.GetoptError:
        print 'test.py -i <inputfile> -o <outputfile>'
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print 'createRandomMatrix.py -n <n> -t <true | false> -l <label> -v'
            sys.exit()
        elif opt in ("-n"):
            n_str = arg
        elif opt in ("-t", "--transpose"):
            transpose = 1
        elif opt in ("-l", "--label"):
            label = arg
        elif opt == '-v':
            verbose = 1
        elif opt in ("-a", "--algo"):
            algo = arg

    n_int = int(n_str)

    if (algo == "rand"):
        if verbose:
            print '==> creating random Matrix ', label, ' with n = ', n_str, ' (transposed = ', transpose, ')'
        matrix = np.random.rand(n_int, n_int)
    elif (algo == "unit"):
        if verbose:
            print '==> creating unit Matrix ', label, ' with n = ', n_str, ' (transposed = ', transpose, ')'
        matrix = np.identity(n_int)
    elif (algo == "cont"):
        if verbose:
            print '==> creating continous Matrix ', label, ' with n = ', n_str, ' (transposed = ', transpose, ')'

    if (transpose == 1):
        matrix = matrix.transpose()


    if verbose:
        print matrix
        print '==> output:'

# printing matrix to stdput with hadoop format:
    for i in range(n_int):
        print label, ' ', n_str, ' ', i, ' ', ' '.join(map(str, matrix[i]))




if __name__ == "__main__":
    main(sys.argv[1:])
