#!/bin/bash

#alias xls='hdfs dfs -ls'
#alias xrm='hdfs dfs -rm'
#alias xcp='hdfs dfs -copyFromLocal'
#alias xmkdir='hdfs dfs -mkdir'
#alias xcat='hdfs dfs -cat'

n_list=(2 4 10 25 50 100 250 500 1000 10000)
n_list_test=(2 5)

rm -f benchmark.txt
#touch benchmark.txt

for n in "${n_list[@]}"; do
	printf "==> creating ranom matrix with n %s\n" "$n"
	./createRandomMatrix.py -n $n -a rand -l A > benchmark.matrix
	./createRandomMatrix.py -n $n -a rand -l B >> benchmark.matrix

	printf "==> removing old and copiing new matrix\n"
	hdfs dfs -rm matrix_in/* &> /dev/null
	hdfs dfs -copyFromLocal benchmark.matrix matrix_in/. &> /dev/null

	cd MatrixMultiV1

	if (( n < 501)); then
		printf "==> STARTING calculation with n = %s and ALGO_1\n" "$n"
	        START=$(date +%s.%N)
        	./compileAndRun.sh &> /dev/null
       	 	END=$(date +%s.%N)
        	DIFF1=$(echo "$END - $START" | bc)
        	printf "==> FINISED calculation with ALGO_1 in %s seconds\n" "$DIFF1"
	else
		DIFF1=0
	fi

	cd ..
	cd MatrixMultiV2

	printf "==> STARTING calculation with n = %s and ALGO_2\n" "$n"
        START=$(date +%s.%N)
        ./compileAndRun.sh &> /dev/null
        END=$(date +%s.%N)
        DIFF2=$(echo "$END - $START" | bc)
        printf "==> FINISED calculation with ALGO_1 in %s seconds\n" "$DIFF2"

	cd ..
	printf "%s\t%s\t%s\n" "$n" "$DIFF1" "$DIFF2" >> benchmark.txt

done
