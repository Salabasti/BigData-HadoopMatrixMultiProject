#!/bin/bash
javac -classpath /usr/lib/hadoop-mapreduce/*:/usr/lib/hadoop/* -d MatrixMultiV2_classes/1 MatrixMultiV2_1.java
javac -classpath /usr/lib/hadoop-mapreduce/*:/usr/lib/hadoop/* -d MatrixMultiV2_classes/2 MatrixMultiV2_2.java
jar -cvf MatrixMultiV2_1.jar -C MatrixMultiV2_classes/1 .
jar -cvf MatrixMultiV2_2.jar -C MatrixMultiV2_classes/2 .
hdfs dfs -rm -r matrix_tmp
hdfs dfs -rm -r matrix_out
hadoop jar MatrixMultiV2_1.jar org.myorg.MatrixMultiV2_1 matrix_in matrix_tmp
hadoop jar MatrixMultiV2_2.jar org.myorg.MatrixMultiV2_2 matrix_tmp matrix_out
