package org.myorg;
 	
import java.io.IOException;
import java.util.*;
 	
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;
 	
public class MatrixMultiV2_2 {
 	
   public static class Map1 extends MapReduceBase implements Mapper<LongWritable, Text, Text, Text> {

       private Text key_out = new Text();
       private Text val_out = new Text();
       
       
       public void map(LongWritable key, Text value, OutputCollector<Text, Text> output, Reporter reporter) throws IOException {
           String line = value.toString();
         
           String[] parts = line.split("\\s+");
           
           String i_str = parts[0];
           String j_str = parts[1];
           String val_str = parts[2];
           
           key_out.set(i_str + " " + j_str);
           val_out.set(val_str);
           output.collect(key_out, val_out);
       }
   }
 	
    public static class Reduce1 extends MapReduceBase implements Reducer<Text, Text, Text, Text> {
        
        private Text val_out = new Text();
        
        
        public void reduce(Text key, Iterator<Text> values, OutputCollector<Text, Text> output, Reporter reporter) throws IOException {
            
            double sum = 0;
            while (values.hasNext()) {
                sum += Double.parseDouble(values.next().toString());
            }
            
            val_out.set("" + sum);
            output.collect(key, val_out);
        }
    }
    
 	
   public static void main(String[] args) throws Exception {
     JobConf conf1 = new JobConf(MatrixMultiV2_2.class);
     conf1.setJobName("matrix_multi_v2_2");
 	
     conf1.setOutputKeyClass(Text.class);
     conf1.setOutputValueClass(Text.class);

     conf1.setMapperClass(Map1.class);
     //conf.setCombinerClass(Reducer.class);
     conf1.setReducerClass(Reduce1.class);
 	
     conf1.setInputFormat(TextInputFormat.class);
     conf1.setOutputFormat(TextOutputFormat.class);
 	
     FileInputFormat.setInputPaths(conf1, new Path(args[0]));
     FileOutputFormat.setOutputPath(conf1, new Path(args[1]));
 	
     JobClient.runJob(conf1);
   }
}
