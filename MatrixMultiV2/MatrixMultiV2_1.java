package org.myorg;
 	
import java.io.IOException;
import java.util.*;
 	
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;
 	
public class MatrixMultiV2_1 {
 	
   public static class Map1 extends MapReduceBase implements Mapper<LongWritable, Text, Text, Text> {

       private Text key_out = new Text();
       private Text val_out = new Text();
       
       
       public void map(LongWritable key, Text value, OutputCollector<Text, Text> output, Reporter reporter) throws IOException {
           String line = value.toString();
         
           String[] parts = line.split("\\s+");
           
           String label = parts[0];
           
           String n_str = parts[1];
           int n = Integer.parseInt(n_str);
           
           String i_str = parts[2];
               
           for (int k = 0; k < n; ++k) {
               String key_out_str = "" + k;
               String val_out_str = label + " " + i_str + " " + parts[3 + k];
                   
               key_out.set(key_out_str);
               val_out.set(val_out_str);
               output.collect(key_out, val_out);
            }
       }
   }
 	
    public static class Reduce1 extends MapReduceBase implements Reducer<Text, Text, Text, Text> {
        
        private Text key_out = new Text();
        private Text val_out = new Text();
        
        
        public void reduce(Text key, Iterator<Text> values, OutputCollector<Text, Text> output, Reporter reporter) throws IOException {
            List<String> cache = new ArrayList<String>();
            
            while (values.hasNext()) {
                cache.add(values.next().toString());
            }
            
            final int n = cache.size() / 2;
            
            Collections.sort(cache);
            
            for (int i = 0; i < n; ++i) {
                for (int j = 0; j < n; ++j) {
                    String key_out_str = i + " " + j;
                    
                    String matrix_1_val_str = cache.get(0 + i);
                    String matrix_2_val_str = cache.get(n + j);
                    String[] matrix_1_val_parts = matrix_1_val_str.split("\\s+");
                    String[] matrix_2_val_parts = matrix_2_val_str.split("\\s+");
                    
                    double matrix_1_val = Double.parseDouble(matrix_1_val_parts[2]);
                    double matrix_2_val = Double.parseDouble(matrix_2_val_parts[2]);
                    
                    String val_out_str = "" + matrix_1_val * matrix_2_val;
                    
                    key_out.set(key_out_str);
                    val_out.set(val_out_str);
                    output.collect(key_out, val_out);
                }
            }
        }
    }
    
 	
   public static void main(String[] args) throws Exception {
     JobConf conf1 = new JobConf(MatrixMultiV2_1.class);
     conf1.setJobName("matrix_multi_v2_1");
 	
     conf1.setOutputKeyClass(Text.class);
     conf1.setOutputValueClass(Text.class);

     conf1.setMapperClass(Map1.class);
     //conf.setCombinerClass(Reducer.class);
     conf1.setReducerClass(Reduce1.class);
 	
     conf1.setInputFormat(TextInputFormat.class);
     conf1.setOutputFormat(TextOutputFormat.class);
 	
     FileInputFormat.setInputPaths(conf1, new Path(args[0]));
     FileOutputFormat.setOutputPath(conf1, new Path(args[1]));
 	
     JobClient.runJob(conf1);
   }
}
